package main

import (
	"fmt"
	"os"
	"log"
	"io"
	"net/http"
	"encoding/json"
	"choppy/config"
)

func help() {
	// print some help, and exit the program
	fmt.Println("--- choppy help ---\n")
	fmt.Println("Options")
	fmt.Println("choppy next")
	fmt.Println("choppy play")
	fmt.Println("choppy stop")
	fmt.Println("choppy pause")
	fmt.Println("choppy forward")
	fmt.Println("choppy volume up")
	fmt.Println("choppy volume down")
	fmt.Println("choppy toggle")
	fmt.Println("")
	os.Exit(0)
}

// read the endpoint, print the response body
func getEndpoint(endpoint string) []byte {
	res, err := http.Get(endpoint)
	if err != nil {
		log.Fatal(err)
	}
	body, err := io.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return body
}

func getActionValue() string {
	if (len(os.Args) < 3) {
		help()
	}
	return os.Args[2]
}


func main() {
	endpoint := ""
  //load the config
  conf := config.New()
  baseEndpoint := fmt.Sprintf("http://%s:%s/player/",conf.Hostname, conf.Port)
	
	// is there an arg1?
	if (len(os.Args) < 2) {
		help()
	}
	// arg 1 should be our action
	action := os.Args[1]
	// is the action a request for help?
	if (action=="help" || action == "-h") {
		help()
	}
	
	//start processing the action
	switch action {
		case "next":
			endpoint = baseEndpoint+"next"
		case "play":
			endpoint = baseEndpoint+"play"
		case "stop":
			endpoint = baseEndpoint+"stop"
		case "pause":
			endpoint = baseEndpoint+"pause"
		case "forward":
			endpoint = baseEndpoint+"forward"
			
		case "volume":
			// it is necessary to check Args[2] for a value
			switch getActionValue() {
				case "up":
					endpoint = baseEndpoint+"volume_change/3"
				case "down":
					endpoint = baseEndpoint+"volume_change/-3"
			}
		case "toggle":
			var parsedJson map[string]any
			data := getEndpoint(baseEndpoint+"status")
			json.Unmarshal(data, &parsedJson)
			if (parsedJson["state"]=="PLAYING") {
				endpoint = baseEndpoint+"pause"
			} else {
				endpoint = baseEndpoint+"play"				
			}
	}
	
	// get the endpoint if it is not nil
	if (len(endpoint) > 0) {
		response := getEndpoint(endpoint)
		fmt.Printf("%s\n", response)
	}
}

