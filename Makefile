heybuddy:		
	# Hey buddy!
	#
	# Commands:
	# make build : builds choppy
	
	# make build-arm6 : build for arm6 (Raspberry Pi A+)
	
build:
	cd src && go build  -o ../choppy choppy.go
	
build-arm6:
	cd src && env GOOS=linux GOARCH=arm GOARM=6 go build  -o ../choppy choppy.go

