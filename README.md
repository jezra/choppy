# Choppy
choppy is a command line tool to provide a limited amount of control over a [Muttonchop](https://gitlab.com/jezra/muttonchop) server.

## compile choppy
choppy is written in Go  
run `make build` to compile choppy

## configuring choppy
choppy will look for a config file named "choppyConf.json" in the same directory as choppy,  or in ~/.config/

There are 2 attributes to the config, hostname and port.    
`hostname` is the hostname of the muttonchop server    
`port` is the port the muttonchop server is listening on. This value is a represented as a string in the config

## running choppy

when run with no arguments, or with 'help' or '-h', choppy will print a list of available commands.

### commands

`choppy next` to play the next media file   
`choppy play` to play the currently set media file    
`choppy stop` to stop playing    
`choppy pause` to pause playing    
`choppy forward` to skip forward    
`choppy volume up` to increase the volume    
`choppy volume down` to decrease the volume  
`choppy toggle` to toggle play/pause 
