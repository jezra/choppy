// copyright Jezra 2024
// released GPLv3

package config

import (
  "log"
  "fmt"
  "io/ioutil"
  "encoding/json"
  "path/filepath"
  "os"
  "os/user"
)

//define the Config structure
type Config struct{
  Hostname string
  Port string
}

func New() Config {
	appDir, dErr := filepath.Abs(filepath.Dir(os.Args[0]))
  if dErr != nil {
    log.Fatal(dErr)
  }
  confPath := appDir+"/choppyConf.json"
  /* find the config file */
  data, err := ioutil.ReadFile(confPath)
  if err != nil {
    // look in ~/.config/
    u,_ := user.Current()
    userConfDir, uConfErr := filepath.Abs(u.HomeDir)
    if dErr != nil {
			log.Fatal(uConfErr)
		}
    confPath = userConfDir+"/.config/choppyConf.json"
		data, err = ioutil.ReadFile(confPath)
		if err != nil {
			log.Fatal(err)
		}
  }
  // where was config found?
  fmt.Printf("using config: %v\n\n", confPath)
  //create a var to hold the json object
  var conf Config
  // unmarshall it
  err = json.Unmarshal(data, &conf)
  if err != nil {
    log.Fatal("Config: %s", err)
  }
  //return the configuration
  return conf
}

